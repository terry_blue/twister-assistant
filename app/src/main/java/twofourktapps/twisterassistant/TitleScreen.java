package twofourktapps.twisterassistant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TitleScreen extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.title_screen);
		
		final Button spinnerScreenButton = (Button)findViewById(R.id.button_to_spinner_screen);
		final Button rulesScreenButton = (Button)findViewById(R.id.button_to_rules_screen);
		
		spinnerScreenButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// Start the spinner screen activity
				startActivity(new Intent(getApplicationContext(), SpinnerScreen.class));				
			}
		});
		
		rulesScreenButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// start the rules screen activity
				startActivity(new Intent(getApplicationContext(), RulesScreen.class));
			}
		});
	}
}
