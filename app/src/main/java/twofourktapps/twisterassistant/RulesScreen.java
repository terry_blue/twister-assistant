package twofourktapps.twisterassistant;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;

public class RulesScreen extends Activity {

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rules_layout);
		final ViewFlipper headerFlipper = (ViewFlipper)findViewById(R.id.viewflipper_rules_page_header);
		final ViewFlipper textFlipper = (ViewFlipper)findViewById(R.id.viewflipper_rules_page_content);
		final Button prevButton = (Button)findViewById(R.id.button_gamerules_prev);
		final Button nextButton = (Button)findViewById(R.id.button_gamerules_next);
		prevButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				headerFlipper.showPrevious();
				textFlipper.showPrevious();
				buttonEnablement();
				
			}
		});
		nextButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				headerFlipper.showNext();
				textFlipper.showNext();
				buttonEnablement();
				
			}
		});
		buttonEnablement();
	}
	
	public void buttonEnablement() {
		final ViewFlipper headerFlipper = (ViewFlipper)findViewById(R.id.viewflipper_rules_page_header);
		final Button prevButton = (Button)findViewById(R.id.button_gamerules_prev);
		final Button nextButton = (Button)findViewById(R.id.button_gamerules_next);
		
		switch(headerFlipper.getDisplayedChild()) {
		case 0: {			
			prevButton.setVisibility(View.GONE);
			nextButton.setVisibility(View.VISIBLE);			
			break;
		}
		case 1: {
			prevButton.setVisibility(View.VISIBLE);
			nextButton.setVisibility(View.VISIBLE);	
			break;
		}
		case 2: {
			prevButton.setVisibility(View.VISIBLE);
			nextButton.setVisibility(View.GONE);	
			break;
		}
		}
	}
}
