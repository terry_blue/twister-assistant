package twofourktapps.twisterassistant;

import java.util.Random;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SpinnerScreen extends Activity { 
	
	Random rand;
	Drawable[] circleImageArray;
	Drawable[] bodypartImageArray;
	final int NUM_OF_CIRCLES = 4;
	final int NUM_OF_BODYPARTS = 4;
	
	int curBodypart, curColour;
	boolean fDrawablesSet = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.spinner_screen_layout);
		
		rand = new Random(System.currentTimeMillis());
		setDrawables();
		getNewMove();
		
		LinearLayout picInstructionLayout = (LinearLayout)findViewById(R.id.instruction_picture_layout);
		picInstructionLayout.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				getNewMove();				
			}
		});
	}
	
	public void getNewMove() {
		if(fDrawablesSet) {
			rand.setSeed(System.currentTimeMillis());		
			int newColour, newBodypart;
			do {
				newColour = (rand.nextInt(100) % NUM_OF_CIRCLES);
			} while (newColour == curColour);
			do {
				newBodypart = (rand.nextInt(100) % NUM_OF_BODYPARTS);
			} while (newBodypart == curBodypart);
			
			final ImageView circleImageView = (ImageView)findViewById(R.id.imageview_circle);
			final ImageView bodypartImageView = (ImageView)findViewById(R.id.imageview_bodypart);
			circleImageView.setImageDrawable(circleImageArray[newColour]);
			bodypartImageView.setImageDrawable(bodypartImageArray[newBodypart]);
			curColour = getInt(newColour);
			curBodypart = getInt(newBodypart);
			
			//Set the text instruction
			TextView instructionText = (TextView)findViewById(R.id.textview_instruction);
			String instructionString = "";
			
			switch(curBodypart) {
			case 0: {
				instructionString += "LEFT FOOT ";
				break;
			}
			case 1: {
				instructionString += "RIGHT FOOT ";
				break;
			}
			case 2: {
				instructionString += "LEFT HAND ";
				break;
			}
			case 3: {
				instructionString += "RIGHT HAND ";
				break;
			}
			}
			
			switch (curColour) {
			case 0: {
				instructionString += "BLUE";
				break;
			}
			case 1: {
				instructionString += "RED";
				break;
			}
			case 2: {
				instructionString += "GREEN";
				break;
			}
			case 3: {
				instructionString += "YELLOW";
				break;
			}
			}
			
			instructionText.setText(instructionString);
		}
	}
	
	public void setDrawables() {
		circleImageArray = new Drawable[NUM_OF_CIRCLES];
		bodypartImageArray = new Drawable[NUM_OF_BODYPARTS];
		
		Resources res = getResources();
		//Circle Images
		circleImageArray[0] = res.getDrawable(R.drawable.blue_circle);
		circleImageArray[1] = res.getDrawable(R.drawable.red_circle);
		circleImageArray[2] = res.getDrawable(R.drawable.green_circle);
		circleImageArray[3] = res.getDrawable(R.drawable.yellow_circle);
		
		//Body part images
		bodypartImageArray[0] = res.getDrawable(R.drawable.l_foot);
		bodypartImageArray[1] = res.getDrawable(R.drawable.r_foot);
		bodypartImageArray[2] = res.getDrawable(R.drawable.l_hand);
		bodypartImageArray[3] = res.getDrawable(R.drawable.r_hand);
		
		fDrawablesSet = true;
	}
	
	public int getInt(int value) {
		return value;
	}
}
